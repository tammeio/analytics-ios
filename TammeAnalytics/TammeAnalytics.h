//
//  TammeAnalytics.h
//  TammeAnalytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Analytics.
FOUNDATION_EXPORT double AnalyticsVersionNumber;

//! Project version string for Analytics.
FOUNDATION_EXPORT const unsigned char AnalyticsVersionString[];

#import "TMAnalytics.h"
#import "TMTammeIntegration.h"
#import "TMTammeIntegrationFactory.h"
#import "TMContext.h"
#import "TMMiddleware.h"
