//
//  TMCrypto.h
//  Analytics
//
//  Copyright © 2018 Tamme. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TMCrypto <NSObject>

- (NSData *_Nullable)encrypt:(NSData *_Nonnull)data;
- (NSData *_Nullable)decrypt:(NSData *_Nonnull)data;

@end
