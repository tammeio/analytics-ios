//
//  TMContext.h
//  Analytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMIntegration.h"

typedef NS_ENUM(NSInteger, SEGEventType) {
    // Should not happen, but default state
    SEGEventTypeUndefined,
    // Core Tracking Methods
    SEGEventTypeIdentify,
    SEGEventTypeTrack,
    SEGEventTypeScreen,
    SEGEventTypeGroup,
    SEGEventTypeAlias,

    // General utility
    SEGEventTypeReset,
    SEGEventTypeFlush,

    // Remote Notification
    SEGEventTypeReceivedRemoteNotification,
    SEGEventTypeFailedToRegisterForRemoteNotifications,
    SEGEventTypeRegisteredForRemoteNotifications,
    SEGEventTypeHandleActionWithForRemoteNotification,

    // Application Lifecycle
    SEGEventTypeApplicationLifecycle,
    //    DidFinishLaunching,
    //    SEGEventTypeApplicationDidEnterBackground,
    //    SEGEventTypeApplicationWillEnterForeground,
    //    SEGEventTypeApplicationWillTerminate,
    //    SEGEventTypeApplicationWillResignActive,
    //    SEGEventTypeApplicationDidBecomeActive,

    // Misc.
    SEGEventTypeContinueUserActivity,
    SEGEventTypeOpenURL,

};

@class TMAnalytics;
@protocol SEGMutableContext;


@interface TMContext : NSObject <NSCopying>

// Loopback reference to the top level TMAnalytics object.
// Not sure if it's a good idea to keep this around in the context.
// since we don't really want people to use it due to the circular
// reference and logic (Thus prefixing with underscore). But
// Right now it is required for integrations to work so I guess we'll leave it in.
@property (nonatomic, readonly, nonnull) TMAnalytics *_analytics;
@property (nonatomic, readonly) SEGEventType eventType;

@property (nonatomic, readonly, nullable) NSString *userId;
@property (nonatomic, readonly, nullable) NSString *anonymousId;
@property (nonatomic, readonly, nullable) NSError *error;
@property (nonatomic, readonly, nullable) TMPayload *payload;
@property (nonatomic, readonly) BOOL debug;

- (instancetype _Nonnull)initWithAnalytics:(TMAnalytics *_Nonnull)analytics;

- (TMContext *_Nonnull)modify:(void (^_Nonnull)(id<SEGMutableContext> _Nonnull ctx))modify;

@end

@protocol SEGMutableContext <NSObject>

@property (nonatomic) SEGEventType eventType;
@property (nonatomic, nullable) NSString *userId;
@property (nonatomic, nullable) NSString *anonymousId;
@property (nonatomic, nullable) TMPayload *payload;
@property (nonatomic, nullable) NSError *error;
@property (nonatomic) BOOL debug;

@end
