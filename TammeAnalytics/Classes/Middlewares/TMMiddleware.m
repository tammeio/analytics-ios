//
//  TMMiddleware.m
//  Analytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import "TMUtils.h"
#import "TMMiddleware.h"


@implementation SEGBlockMiddleware

- (instancetype)initWithBlock:(TMMiddlewareBlock)block
{
    if (self = [super init]) {
        _block = block;
    }
    return self;
}

- (void)context:(TMContext *)context next:(TMMiddlewareNext)next
{
    self.block(context, next);
}

@end


@implementation TMMiddlewareRunner

- (instancetype)initWithMiddlewares:(NSArray<id<TMMiddleware>> *_Nonnull)middlewares
{
    if (self = [super init]) {
        _middlewares = middlewares;
    }
    return self;
}

- (void)run:(TMContext *_Nonnull)context callback:(RunMiddlewaresCallback _Nullable)callback
{
    [self runMiddlewares:self.middlewares context:context callback:callback];
}

// TODO: Maybe rename TMContext to SEGEvent to be a bit more clear?
// We could also use some sanity check / other types of logging here.
- (void)runMiddlewares:(NSArray<id<TMMiddleware>> *_Nonnull)middlewares
               context:(TMContext *_Nonnull)context
              callback:(RunMiddlewaresCallback _Nullable)callback
{
    BOOL earlyExit = context == nil;
    if (middlewares.count == 0 || earlyExit) {
        if (callback) {
            callback(earlyExit, middlewares);
        }
        return;
    }

    [middlewares[0] context:context next:^(TMContext *_Nullable newContext) {
        NSArray *remainingMiddlewares = [middlewares subarrayWithRange:NSMakeRange(1, middlewares.count - 1)];
        [self runMiddlewares:remainingMiddlewares context:newContext callback:callback];
    }];
}

@end
