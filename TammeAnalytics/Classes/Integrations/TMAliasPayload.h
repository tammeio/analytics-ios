#import <Foundation/Foundation.h>
#import "TMPayload.h"

NS_ASSUME_NONNULL_BEGIN


@interface TMAliasPayload : TMPayload

@property (nonatomic, readonly) NSString *theNewId;

- (instancetype)initWithNewId:(NSString *)newId
                      context:(JSON_DICT)context
                 integrations:(JSON_DICT)integrations;

@end

NS_ASSUME_NONNULL_END
