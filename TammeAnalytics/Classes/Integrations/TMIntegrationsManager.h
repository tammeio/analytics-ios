//
//  TMIntegrationsManager.h
//  Analytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMMiddleware.h"

/**
 * NSNotification name, that is posted after integrations are loaded.
 */
extern NSString *_Nonnull TMAnalyticsIntegrationDidStart;

@class TMAnalytics;


@interface TMIntegrationsManager : NSObject

// Exposed for testing.
+ (BOOL)isTrackEvent:(NSString *_Nonnull)event enabledForIntegration:(NSString *_Nonnull)key inPlan:(NSDictionary *_Nonnull)plan;

// @Deprecated - Exposing for backward API compat reasons only
@property (nonatomic, readonly) NSMutableDictionary *_Nonnull registeredIntegrations;

- (instancetype _Nonnull)initWithAnalytics:(TMAnalytics *_Nonnull)analytics;

// @Deprecated - Exposing for backward API compat reasons only
- (NSString *_Nonnull)getAnonymousId;

@end


@interface TMIntegrationsManager (TMMiddleware) <TMMiddleware>

@end
