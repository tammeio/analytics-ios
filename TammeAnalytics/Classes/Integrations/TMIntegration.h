#import <Foundation/Foundation.h>
#import "TMIdentifyPayload.h"
#import "TMTrackPayload.h"
#import "TMScreenPayload.h"
#import "TMAliasPayload.h"
#import "TMIdentifyPayload.h"
#import "TMGroupPayload.h"
#import "TMContext.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TMIntegration <NSObject>

@optional
// Identify will be called when the user calls either of the following:
// 1. [[TMAnalytics sharedInstance] identify:someUserId];
// 2. [[TMAnalytics sharedInstance] identify:someUserId traits:someTraits];
// 3. [[TMAnalytics sharedInstance] identify:someUserId traits:someTraits options:someOptions];
// @see https://tamme.io/docs/spec/identify/
- (void)identify:(TMIdentifyPayload *)payload;

// Track will be called when the user calls either of the following:
// 1. [[TMAnalytics sharedInstance] track:someEvent];
// 2. [[TMAnalytics sharedInstance] track:someEvent properties:someProperties];
// 3. [[TMAnalytics sharedInstance] track:someEvent properties:someProperties options:someOptions];
// @see https://tamme.io/docs/spec/track/
- (void)track:(TMTrackPayload *)payload;

// Screen will be called when the user calls either of the following:
// 1. [[TMAnalytics sharedInstance] screen:someEvent];
// 2. [[TMAnalytics sharedInstance] screen:someEvent properties:someProperties];
// 3. [[TMAnalytics sharedInstance] screen:someEvent properties:someProperties options:someOptions];
// @see https://tamme.io/docs/spec/screen/
- (void)screen:(TMScreenPayload *)payload;

// Group will be called when the user calls either of the following:
// 1. [[TMAnalytics sharedInstance] group:someGroupId];
// 2. [[TMAnalytics sharedInstance] group:someGroupId traits:];
// 3. [[TMAnalytics sharedInstance] group:someGroupId traits:someGroupTraits options:someOptions];
// @see https://tamme.io/docs/spec/group/
- (void)group:(TMGroupPayload *)payload;

// Alias will be called when the user calls either of the following:
// 1. [[TMAnalytics sharedInstance] alias:someNewId];
// 2. [[TMAnalytics sharedInstance] alias:someNewId options:someOptions];
// @see https://tamme.io/docs/spec/alias/
- (void)alias:(TMAliasPayload *)payload;

// Reset is invoked when the user logs out, and any data saved about the user should be cleared.
- (void)reset;

// Flush is invoked when any queued events should be uploaded.
- (void)flush;

// App Delegate Callbacks

// Callbacks for notifications changes.
// ------------------------------------
- (void)receivedRemoteNotification:(NSDictionary *)userInfo;
- (void)failedToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void)registeredForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void)handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo;

// Callbacks for app state changes
// -------------------------------

- (void)applicationDidFinishLaunching:(NSNotification *)notification;
- (void)applicationDidEnterBackground;
- (void)applicationWillEnterForeground;
- (void)applicationWillTerminate;
- (void)applicationWillResignActive;
- (void)applicationDidBecomeActive;

- (void)continueUserActivity:(NSUserActivity *)activity;
- (void)openURL:(NSURL *)url options:(NSDictionary *)options;

@end

NS_ASSUME_NONNULL_END
