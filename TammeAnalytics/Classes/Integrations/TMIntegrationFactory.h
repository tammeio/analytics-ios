#import <Foundation/Foundation.h>
#import "TMIntegration.h"
#import "TMAnalytics.h"

NS_ASSUME_NONNULL_BEGIN

@class TMAnalytics;

@protocol TMIntegrationFactory

/**
 * Attempts to create an adapter with the given settings. Returns the adapter if one was created, or null
 * if this factory isn't capable of creating such an adapter.
 */
- (id<TMIntegration>)createWithSettings:(NSDictionary *)settings forAnalytics:(TMAnalytics *)analytics;

/** The key for which this factory can create an Integration. */
- (NSString *)key;

@end

NS_ASSUME_NONNULL_END
