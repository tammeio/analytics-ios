#import <Foundation/Foundation.h>
#import "TMIntegrationFactory.h"
#import "TMHTTPClient.h"
#import "TMStorage.h"

NS_ASSUME_NONNULL_BEGIN


@interface TMTammeIntegrationFactory : NSObject <TMIntegrationFactory>

@property (nonatomic, strong) TMHTTPClient *client;
@property (nonatomic, strong) id<TMStorage> storage;

- (instancetype)initWithHTTPClient:(TMHTTPClient *)client storage:(id<TMStorage>)storage;

@end

NS_ASSUME_NONNULL_END
