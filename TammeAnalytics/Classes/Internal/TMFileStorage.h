//
//  TMFileStorage.h
//  Analytics
//
//  Copyright © 2018 Tamme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMStorage.h"


@interface TMFileStorage : NSObject <TMStorage>

@property (nonatomic, strong, nullable) id<TMCrypto> crypto;

- (instancetype _Nonnull)init;
- (instancetype _Nonnull)initWithFolder:(NSURL *_Nonnull)folderURL crypto:(id<TMCrypto> _Nullable)crypto;

- (NSURL *_Nonnull)urlForKey:(NSString *_Nonnull)key;

+ (NSURL *_Nullable)applicationSupportDirectoryURL;

@end
