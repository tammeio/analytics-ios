#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "TMAnalytics.h"

NS_ASSUME_NONNULL_BEGIN


@interface TMStoreKitTracker : NSObject <SKPaymentTransactionObserver, SKProductsRequestDelegate>

+ (instancetype)trackTransactionsForAnalytics:(TMAnalytics *)analytics;

@end

NS_ASSUME_NONNULL_END
