//
//  TMUserDefaultsStorage.h
//  Analytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMStorage.h"


@interface TMUserDefaultsStorage : NSObject <TMStorage>

@property (nonatomic, strong, nullable) id<TMCrypto> crypto;
@property (nonnull, nonatomic, readonly) NSUserDefaults *defaults;
@property (nullable, nonatomic, readonly) NSString *namespacePrefix;

- (instancetype _Nonnull)initWithDefaults:(NSUserDefaults *_Nonnull)defaults namespacePrefix:(NSString *_Nullable)namespacePrefix crypto:(id<TMCrypto> _Nullable)crypto;

@end
