#import <Foundation/Foundation.h>
#import "TMIntegration.h"
#import "TMHTTPClient.h"
#import "TMStorage.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString *const SEGtammeDidSendRequestNotification;
extern NSString *const SEGtammeRequestDidSucceedNotification;
extern NSString *const SEGtammeRequestDidFailNotification;


@interface TMTammeIntegration : NSObject <TMIntegration>

- (id)initWithAnalytics:(TMAnalytics *)analytics httpClient:(TMHTTPClient *)httpClient storage:(id<TMStorage>)storage;

@end

NS_ASSUME_NONNULL_END
