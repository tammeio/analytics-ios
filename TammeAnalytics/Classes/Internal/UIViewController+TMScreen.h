#import <UIKit/UIKit.h>


@interface UIViewController (TMScreen)

+ (void)seg_swizzleViewDidAppear;
+ (UIViewController *)seg_topViewController;

@end
