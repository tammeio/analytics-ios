#import "TMTammeIntegrationFactory.h"
#import "TMTammeIntegration.h"


@implementation TMTammeIntegrationFactory

- (id)initWithHTTPClient:(TMHTTPClient *)client storage:(id<TMStorage>)storage
{
    if (self = [super init]) {
        _client = client;
        _storage = storage;
    }
    return self;
}

- (id<TMIntegration>)createWithSettings:(NSDictionary *)settings forAnalytics:(TMAnalytics *)analytics
{
    return [[TMTammeIntegration alloc] initWithAnalytics:analytics httpClient:self.client storage:self.storage];
}

- (NSString *)key
{
    return @"tamme.io";
}

@end
