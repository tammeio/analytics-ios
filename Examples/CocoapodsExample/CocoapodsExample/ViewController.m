//
//  ViewController.m
//  CocoapodsExample
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import <TammeAnalytics/TMAnalytics.h>
// TODO: Test and see if this works
// @import Analytics;
#import "ViewController.h"


@interface ViewController ()

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSUserActivityTypeBrowsingWeb];
    userActivity.webpageURL = [NSURL URLWithString:@"http://www.tamme.io"];
    [[TMAnalytics sharedAnalytics] continueUserActivity:userActivity];
    [[TMAnalytics sharedAnalytics] track:@"test"];
    [[TMAnalytics sharedAnalytics] flush];
}

- (IBAction)fireEvent:(id)sender
{
    [[TMAnalytics sharedAnalytics] track:@"Cocoapods Example Button"];
    [[TMAnalytics sharedAnalytics] flush];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
