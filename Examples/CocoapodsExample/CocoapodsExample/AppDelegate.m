//
//  AppDelegate.m
//  CocoapodsExample
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import <TammeAnalytics/TMAnalytics.h>
#import "AppDelegate.h"


@interface AppDelegate ()

@end

// https://tamme.io/tamme-mobile/sources/ios_cocoapods_example/overview
NSString *const tamme_WRITE_KEY = @"zr5x22gUVBDM3hO3uHkbMkVe6Pd6sCna";


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [TMAnalytics debug:YES];
    TMAnalyticsConfiguration *configuration = [TMAnalyticsConfiguration configurationWithWriteKey:tamme_WRITE_KEY];
    configuration.trackApplicationLifecycleEvents = YES;
    configuration.trackAttributionData = YES;
    configuration.flushAt = 1;
    [TMAnalytics setupWithConfiguration:configuration];
    [[TMAnalytics sharedAnalytics] track:@"Cocoapods Example Launched"];
    [[TMAnalytics sharedAnalytics] flush];
    NSLog(@"application:didFinishLaunchingWithOptions: %@", launchOptions);
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"applicationWillResignActive:");
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"applicationDidEnterBackground:");
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"applicationWillEnterForeground:");
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"applicationDidBecomeActive:");
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"applicationWillTerminate:");
}

@end
