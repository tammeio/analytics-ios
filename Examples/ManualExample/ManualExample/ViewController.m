//
//  ViewController.m
//  ManualExample
//

//  Copyright © 2018 Tamme. All rights reserved.
//

#import <Analytics/Analytics.h>
#import "ViewController.h"


@interface ViewController ()

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[TMAnalytics sharedAnalytics] track:@"Manual Example Main View Loaded"];
    [[TMAnalytics sharedAnalytics] flush];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)fireEvent:(id)sender
{
    [[TMAnalytics sharedAnalytics] track:@"Manual Example Fire Event"];
    [[TMAnalytics sharedAnalytics] flush];
}

@end
