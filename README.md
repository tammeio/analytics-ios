# Analytics


analytics-ios is an iOS client for tamme.

## Installation

Analytics is available through [CocoaPods](http://cocoapods.org) and [Carthage](https://github.com/Carthage/Carthage).

### CocoaPods

```ruby
pod "TammeAnalytics", "1.0.7"
```

### Carthage

```
git "https://bitbucket.org/tammeio/analytics-ios"
```


## Documentation

More detailed documentation is available at [https://tammeio.atlassian.net/wiki](https://tammeio.atlassian.net/wiki).
