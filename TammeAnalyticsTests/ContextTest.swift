//
//  ContextTest.swift
//  Analytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//

import Quick
import Nimble
import SwiftTryCatch
import Analytics

class ContextTests: QuickSpec {
  override func spec() {
    
    var analytics: TMAnalytics!
    
    beforeEach {
      let config = TMAnalyticsConfiguration(writeKey: "foobar")
      analytics = TMAnalytics(configuration: config)
    }
    
    it("throws when used incorrectly") {
      var context: TMContext?
      var exception: NSException?
      
      SwiftTryCatch.tryRun({
        context = TMContext()
      }, catchRun: { e in
        exception = e
      }, finallyRun: nil)
      
      expect(context).to(beNil())
      expect(exception).toNot(beNil())
    }

    
    it("initialized correctly") {
      let context = TMContext(analytics: analytics)
      expect(context._analytics) == analytics
      expect(context.eventType) == SEGEventType.undefined
    }
    
    it("accepts modifications") {
      let context = TMContext(analytics: analytics)
      
      let newContext = context.modify { context in
        context.userId = "sloth"
        context.eventType = .track;
      }
      expect(newContext.userId) == "sloth"
      expect(newContext.eventType) == SEGEventType.track;
      
    }
    
    it("modifies copy in debug mode to catch bugs") {
      let context = TMContext(analytics: analytics).modify { context in
        context.debug = true
      }
      expect(context.debug) == true
      
      let newContext = context.modify { context in
        context.userId = "123"
      }
      expect(context) !== newContext
      expect(newContext.userId) == "123"
      expect(context.userId).to(beNil())
    }
    
    it("modifies self in non-debug mode to optimize perf.") {
      let context = TMContext(analytics: analytics).modify { context in
        context.debug = false
      }
      expect(context.debug) == false
      
      let newContext = context.modify { context in
        context.userId = "123"
      }
      expect(context) === newContext
      expect(newContext.userId) == "123"
      expect(context.userId) == "123"
    }
    
  }
  
}
