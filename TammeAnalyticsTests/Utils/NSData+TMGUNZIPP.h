//
//  NSData+TMGUNZIPP.h
//  Analytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//
// https://github.com/nicklockwood/GZIP/blob/master/GZIP/NSData%2BGZIP.m

#import <Foundation/Foundation.h>


@interface NSData (TMGUNZIPP)

- (NSData *_Nullable)seg_gunzippedData;

@end
