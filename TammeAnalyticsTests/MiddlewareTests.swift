//
//  MiddlewareTests.swift
//  Analytics
//

//  Copyright © 2018 Tamme. All rights reserved.
//


import Quick
import Nimble
import Analytics

// Changing event names and adding custom attributes
let customizeAllTrackCalls = SEGBlockMiddleware { (context, next) in
  if context.eventType == .track {
    next(context.modify { ctx in
      guard let track = ctx.payload as? TMTrackPayload else {
        return
      }
      let newEvent = "[New] \(track.event)"
      var newProps = track.properties ?? [:]
      newProps["customAttribute"] = "Hello"
      ctx.payload = TMTrackPayload(
        event: newEvent,
        properties: newProps,
        context: track.context,
        integrations: track.integrations
      )
    })
  } else {
    next(context)
  }
}

// Simply swallows all calls and does not pass events downstream
let eatAllCalls = SEGBlockMiddleware { (context, next) in
}

class MiddlewareTests: QuickSpec {
  override func spec() {
    it("receives events") {
      let config = TMAnalyticsConfiguration(writeKey: "TESTKEY")
      let passthrough = SEGPassthroughMiddleware()
      config.middlewares = [
        passthrough,
      ]
      let analytics = TMAnalytics(configuration: config)
      analytics.identify("testUserId1")
      expect(passthrough.lastContext?.eventType) == SEGEventType.identify
      let identify = passthrough.lastContext?.payload as? TMIdentifyPayload
      expect(identify?.userId) == "testUserId1"
    }

    it("modifies and passes event to next") {
      let config = TMAnalyticsConfiguration(writeKey: "TESTKEY")
      let passthrough = SEGPassthroughMiddleware()
      config.middlewares = [
        customizeAllTrackCalls,
        passthrough,
      ]
      let analytics = TMAnalytics(configuration: config)
      analytics.track("Purchase Success")
      expect(passthrough.lastContext?.eventType) == SEGEventType.track
      let track = passthrough.lastContext?.payload as? TMTrackPayload
      expect(track?.event) == "[New] Purchase Success"
      expect(track?.properties?["customAttribute"] as? String) == "Hello"
    }

    it("expects event to be swallowed if next is not called") {
      let config = TMAnalyticsConfiguration(writeKey: "TESTKEY")
      let passthrough = SEGPassthroughMiddleware()
      config.middlewares = [
        eatAllCalls,
        passthrough,
      ]
      let analytics = TMAnalytics(configuration: config)
      analytics.track("Purchase Success")
      expect(passthrough.lastContext).to(beNil())
    }
  }
}
