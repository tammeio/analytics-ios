Pod::Spec.new do |s|
  s.name             = "TammeAnalytics"
  s.version          = "1.0.7"
  s.summary          = "The easiest way to add Tamme to your iOS app"

  s.description      = <<-DESC
                       TammeAnalytics allows you to integrate directly with the
                       tamme.io platform for double sided marketplaces.
                       DESC

  s.homepage         = "http://tamme.io/"
  s.license          =  { :type => 'MIT' }
  s.author           = { "tamme" => "opensource@tamme.io" }
  s.source           = { :git => "https://bitbucket.org/tammeio/analytics-ios.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/meet_tamme'

  s.ios.deployment_target = '7.0'
  s.tvos.deployment_target = '9.0'

  s.frameworks = 'CoreTelephony', 'Security', 'StoreKit', 'SystemConfiguration', 'UIKit'

  s.source_files = [
    'TammeAnalytics/Classes/**/*',
    'TammeAnalytics/Vendor/**/*'
  ]
end
